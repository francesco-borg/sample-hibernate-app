<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
	<jsp:include page="head.jsp">
		<jsp:param name="pageTitle" value="chat with us"/>
	</jsp:include>
</head>
<body>
	<jsp:include page="navbar.jsp"/>
	<c:choose>
		<c:when test= "${sessionId == null }">
			<p>${warningMessageForFrontEnd}</p>		
		</c:when>
		<c:otherwise>
		<input type="hidden" value= "${loggedInUser}" id="loggedInUser"></input>
			<div class="container">
    			<button id="connectBtn" class="btn btn-success" type="submit">Connect</button>
        		<button id="disconnectBtn" class="btn btn-danger" type="submit" disabled="disabled">Disconnect
        		</button>
        		<div class="senderChatArea">
        			<textarea id="senderChatbox" rows= 10 cols="60" readonly></textarea>
        		</div>
				<hr>
				<div class="recipientChatArea">
					<textarea name="chatMessage" 
					rows= "10" cols= "60" id="chatMessage" disabled="disabled"></textarea>
					<button class="btn btn-success" id="sendBtn">Enter</button>							
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</body>
</html>