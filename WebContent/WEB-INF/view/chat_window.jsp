<jsp:include page="head.jsp">
	<jsp:param name="pageTitle" value="chat"/>
</jsp:include>
<div class="container">
	<button id="connectBtn" class="btn btn-success" type="submit">Connect</button>
    <button id="disconnectBtn" class="btn btn-danger" type="submit" disabled="disabled">Disconnect</button>
    <div class="senderChatArea">
    	<textarea id="senderChatbox" rows= 10 cols="60" readonly></textarea>
    </div>
	<hr>
	<div class="recipientChatArea">
		<textarea name="chatMessage" 
				rows= "10" cols= "60" id="chatMessage" disabled="disabled"></textarea>
		<button class="btn btn-success" id="sendBtn">Enter</button>							
		</div>
</div>