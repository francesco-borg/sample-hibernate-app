<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
	<jsp:include page="head.jsp">
		<jsp:param name="pageTitle" value="user list"/>
	</jsp:include>
</head>
<body>
	<jsp:include page="navbar.jsp"/>
	<h1 class= "errorText">Error</h1>
	<h5 class= "errorText">${errorMessageForFrontEnd}</h5>
</body>
</html>