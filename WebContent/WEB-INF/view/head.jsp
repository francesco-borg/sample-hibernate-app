<head>
	<meta charset="ISO-8859-1">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  	<script src="${pageContext.request.contextPath}/assets/js/script.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.3.0/sockjs.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/styles.css">
	<link rel="stylesheet" 
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<title>${param.pageTitle}</title>
</head>
