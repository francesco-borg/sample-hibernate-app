<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp">
		<jsp:param name="pageTitle" value="homepage"/>
	</jsp:include>
</head>
<body>
	<jsp:include page="navbar.jsp"/>
		<c:choose>
    		<c:when test="${sessionId != null}">
        		<h1 id="websiteText">Hello ${loggedInUser}</h1>
        	</c:when>
		</c:choose>
</body>
</html>