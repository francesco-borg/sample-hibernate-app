<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp">
		<jsp:param name="pageTitle" value="login"/>
	</jsp:include>
</head>
<body>
	<jsp:include page="navbar.jsp"/>
	<c:choose>
		<c:when test="${userLoggedIn == false}">
			<div class="alert alert-warning alert-dismissible fade show" id="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				You entered your username/password wrong, or your account don't exist.
			</div>
		</c:when>
	</c:choose>
	<form method="POST" action="loginUser" modelAttribute="loginUserForm">
		<div class="mx-auto" id="loginForm">
			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" required>
			</div>
		
			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" required>	
			</div>
			<button type="submit" class="btn btn-light" id="login">Login!</button>
		</div>
	</form>
</body>
</html>