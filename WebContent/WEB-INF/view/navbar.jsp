<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
      		<li class="nav-item active">
        		<c:choose>
        			<c:when test="${sessionId != null}">
        				<a class="nav-link disabled" href="${pageContext.request.contextPath}/register">Register</a>
        			</c:when>
        			<c:otherwise>
        				<a class="nav-link" href="${pageContext.request.contextPath}/register">Register</a>
        			</c:otherwise>
        		</c:choose>
      		</li>
      		<li class="nav-item">
        		<c:choose>
        			<c:when test="${sessionId != null}">
        				<a class="nav-link" href="${pageContext.request.contextPath}/logout">Logout</a>
        			</c:when>
        			<c:otherwise>
        				<a class="nav-link" href="${pageContext.request.contextPath}/login">Login</a>
        			</c:otherwise>
        		</c:choose>
      		</li>
      		<li class="nav-item">
				<a class="nav-link" href="${pageContext.request.contextPath}/">Home</a>
      		</li>
      		<li class="nav-item">
      			<c:choose>
      				<c:when test="${sessionId == null }">
      					<a class="nav-link disabled" href="${pageContext.request.contextPath}/getUsers">User's list</a>
      				</c:when>
      				<c:otherwise>
      					<a class="nav-link" href="${pageContext.request.contextPath}/getUsers">User's list</a>
      				</c:otherwise>
      			</c:choose>
      		</li>
      		
      		<li class="nav-item">
      			<c:choose>
      				<c:when test="${sessionId == null }">
      					<a class="nav-link disabled" href="${pageContext.request.contextPath}/chat">Chat with us</a>
      				</c:when>	
      				<c:otherwise>
      					<a class="nav-link" href="${pageContext.request.contextPath}/chat">Chat with us</a>
      				</c:otherwise>
      			</c:choose>
      		</li>
      	</ul>

	</div>
</nav>