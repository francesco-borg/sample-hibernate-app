<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp">
		<jsp:param name="pageTitle" value="register"/>
	</jsp:include>
</head>
<body>
	<jsp:include page="navbar.jsp"/>
	<c:choose>
		<c:when test="${userRegistered == true}">
			<div class="alert alert-success alert-dismissible fade show" id="alert">
  				You have registered successfully
			</div>
		</c:when>
		<c:when test="${userRegistered == false }">
			<div class="alert alert-warning-dismissible fade show" id="alert">
				The entered username already exists! or if it's not the error it's something in our end
			</div>
		</c:when>
	</c:choose>
		
	<form method="POST" action="register" modelAttribute="registerUserForm">
		<div class="mx-auto" id="registerForm">
			<div class="form-group" id="formData">
				<label>First Name</label>
				<input type="text" name="firstName" id="firstName" required>	
			</div>
			
			<div class="form-group" id="formData">
				<label>Last Name</label>
				<input type="text" name="lastName" id="lastName" required>	
			</div>
			
			<div class="form-group" id="formData">
				<label>Country</label>
				<input type="text" name="country" id="country">	
			</div>
			
			<div class="form-group" id="formData">
				<label>Username</label>
				<input type="text" name="username" id="username" required>	
			</div>
			
			<div class="form-group" id="formData">
				<label>Password</label>
				<input type="password" name="password" id="password" required>	
			</div>
			<button type="submit" class="btn btn-light">Register</button>
		</div>
	</form>
</body>
</html>