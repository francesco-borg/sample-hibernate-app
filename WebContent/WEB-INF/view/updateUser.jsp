<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="head.jsp">
		<jsp:param name="pageTitle" value="register"/>
	</jsp:include>
</head>
<body>
	<jsp:include page="navbar.jsp"/>


	
	<form method="POST" action="updateUser/${userToBeUpdated.userID}" modelAttribute="updateUserForm" items="${userToBeUpdated}">
		<div class="mx-auto" id="updateUser">
			<div class="form-group">
				<label>First Name</label>
				<input type="text" name="firstName" value="${userToBeUpdated.firstName }">	
			</div>
			
			<div class="form-group">
				<label>Last Name</label>
				<input type="text" name="lastName" value="${userToBeUpdated.lastName }">	
			</div>
			
			<div class="form-group">
				<label>Country</label>
				<input type="text" name="country" value="${userToBeUpdated.country }">	
			</div>
			
			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" value="${userToBeUpdated.username }">	
			</div>
		</div>
			<button type="submit" class="btn btn-light">Update</button>
	</form>


</body>
</html>