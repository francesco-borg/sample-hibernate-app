<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
	<jsp:include page="head.jsp">
		<jsp:param name="pageTitle" value="user list"/>
	</jsp:include>
</head>
<body>
	<jsp:include page="navbar.jsp"/>
	<p>${warningMessageForFrontEnd}</p>	
	<table class="table table-borderless">
		<tr>
			<th>User ID</th>
			<th>Username</th>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Country</th>
		</tr>
		<c:forEach var="tempUser" items="${users}">
			<tr>
				<td>${tempUser.userID}</td>
				<td>${tempUser.username}</td>
				<td>${tempUser.firstName}</td>
				<td>${tempUser.lastName}</td>
				<td>${tempUser.country}</td>
				<td>
					<c:set var="adminVar" value= "admin" />
					<c:choose>
						<c:when test = "${loggedInUser == tempUser.username || userRole == adminVar }">
							<a href="${pageContext.request.contextPath}/viewUser/${tempUser.userID}" class="btn btn-info" >Update user</a>
						</c:when>
					</c:choose>
				</td>
				<td>
					<c:choose>
						<c:when test = "${loggedInUser == tempUser.username || userRole == adminVar }">
							<a href="${pageContext.request.contextPath}/deleteUser/${tempUser.userID}" class="btn btn-info" >Delete user</a>
						</c:when>						
					</c:choose>
					
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>