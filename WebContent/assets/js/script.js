var public_chat_stompClient = null;
var private_chat_stompClient = null;

$(document).ready (function(){
	$("#alert").fadeTo(3000, 0).slideUp(500, function(){
    $("#alert").slideUp(500);
    });   
});

$(document).ready(function (registrationUrl) {
	
	//var registrationFormData = $('#formData');
	
	//When the user submit the registration form this event handler will be triggered
	$('#registrationForm').submit(function(e) {
		var registrationForm = this;
		e.preventDefault();
		var user = {}
		/*var firstName = $('#firstName').val();
		console.log(firstName);
		var lastName = $('#lastName').val();
		var country = $('#country').val();
		var username = $('#username').val();
		var password = $('#password').val();
		var user = {"firstName": firstName, "lastName": lastName, "username": username, "password": password, "country": country };*/
		
		$.each(this, function(i,v){
			var currentInput = $(v);
			user[currentInput.attr("name")] = currentInput.val();
		});
				
		$.ajax({
			type: 'POST',
			contentType: "application/json",
			url: 'register',
			data: JSON.stringify(user),
			success: function(user) {
				return JSON.stringify(user);
			},
			error: function(err) {
				console.log('error');
				console.log(err);
			}
		});	
	});
});

function connect() {
   var public_chat_socket = new SockJS('/sampHibernateApplication/chat');
   public_chat_stompClient = Stomp.over(public_chat_socket);  
   public_chat_stompClient.connect({}, whenConnected, whenError); 
};

function secureConnect() {
	var private_chat_socket = new SockJS('/sampHibernateApplication/private_chat');
	private_chat_stompClient = Stomp.over(private_chat_socket);  
	private_chat_stompClient.connect({}, secureWhenConnected, whenError);
	 
}

function secureWhenConnected() {
	var userToBeloggedIn = {
			from_username: $('#loggedInUser').val(),
			message: $('#loggedInUser').val() + ' has joined the private conversation.',
			msgProtocol: 'JOIN'
	};
	private_chat_stompClient.subscribe('/queue/chatuser', whenMessageReceived);
	private_chat_stompClient.send('/sampHibernateApplication/chatRegister', 
			{}, JSON.stringify(
					userToBeloggedIn));
}

function whenConnected() {
	var userToBeloggedIn = {
			from_username: $('#loggedInUser').val(),
			message: $('#loggedInUser').val() + ' has joined the conversation.',
			msgProtocol: 'JOIN'
	};
	
	
	public_chat_stompClient.subscribe('/chat_topic/public', whenMessageReceived);
	public_chat_stompClient.send('/sampHibernateApplication/chatRegister', 
			{}, JSON.stringify(
					userToBeloggedIn));
	$('#connectBtn').prop('disabled', true);
	$('#disconnectBtn').prop('disabled', false);
	$('#chatMessage').prop('disabled', false); 
}

function disconnect() {
	if (public_chat_stompClient !== null) {
		var userToBeLoggedOut = {
				from_username: $('#loggedInUser').val(),
				message: $('#loggedInUser').val()  + ' has left the conversation.',
				msgProtocol: 'OFFLINE'
		   };
	public_chat_stompClient.send("/sampHibernateApplication/chatSend", 
				{}, JSON.stringify(userToBeLoggedOut));	
	public_chat_stompClient.disconnect();
	}
	//$("#" + userToBeLoggedOut.recipientUser).remove();
	$('#connectBtn').prop('disabled', false);
	$('#disconnectBtn').prop('disabled', true);
	$('#chatMessage').val('');
	$('#chatMessage').prop('disabled', true);

};

function sendMess() {
   var sentMess = {
		   message: $('#chatMessage').val(), 
		   from_username: $('#loggedInUser').val(),
		   msgProtocol: 'ONLINE'
   };
	if(public_chat_stompClient != null) {
		public_chat_stompClient.send("/sampHibernateApplication/chatSend", {}, JSON.stringify(sentMess));
		$('#chatMessage').val('');
	} else if(private_chat_stompClient != null) {
		private_chat_stompClient.send("/sampHibernateApplication/private_chat_room", {}, JSON.stringify(sentMess));
		$('#chatMessage').val('');
	}
};

//the payload here will contain the typed message in JSON
function whenMessageReceived(payload) {
	var convertedMess = JSON.parse(payload.body);
	  if(convertedMess.chat_type.chat_type === 'ONLINE' && convertedMess.message != '') {
		  $('#senderChatbox').append('[' + convertedMess.from_username + ']' 
					+ convertedMess.message + "\n");
	} else if(convertedMess.chat_type.chat_type === 'JOIN') {
		$('#senderChatbox').append(convertedMess.from_username + 
		' has joined the conversation. \n');
	//	$('.oneToOneChatArea').append("<p id=" + convertedMess.recipientUser  + ">" + 
		//		convertedMess.recipientUser + "</p>");
	} else if(convertedMess.chat_type.chat_type === 'OFFLINE') {
		$('#senderChatbox').append(convertedMess.message + "\n");
		$("#" + convertedMess.from_username).remove();
	}
	
}

function whenError(err) {
	console.log(err);
}

$(function () {
   $( "#connectBtn" ).click(function() { connect(); });
   $("#privateChatBtn").click(function() { secureConnect(); });
   $( "#disconnectBtn" ).click(function() { disconnect(); });
   $( "#sendBtn" ).click(function() { sendMess(); });
});