-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: sampleapp
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_chat_log`
--

DROP TABLE IF EXISTS `tbl_chat_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbl_chat_log` (
  `chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_username` varchar(80) NOT NULL,
  `to_username` varchar(80) DEFAULT NULL,
  `chat_message` varchar(250) NOT NULL,
  `date_time` datetime DEFAULT NULL,
  `chat_type_id` int(11) NOT NULL,
  PRIMARY KEY (`chat_id`),
  KEY `chat_type_FK_idx` (`chat_type_id`),
  CONSTRAINT `chat_type_FK` FOREIGN KEY (`chat_type_id`) REFERENCES `tbl_chat_type` (`chat_type_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=202 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_chat_log`
--

LOCK TABLES `tbl_chat_log` WRITE;
/*!40000 ALTER TABLE `tbl_chat_log` DISABLE KEYS */;
INSERT INTO `tbl_chat_log` VALUES (1,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-19 05:54:34',1),(2,'test',NULL,'test has joined the conversation.','2019-07-19 05:54:36',1),(3,'test',NULL,'hello','2019-07-19 05:54:48',2),(4,'fborg001',NULL,'hey','2019-07-19 05:54:52',2),(5,'test',NULL,'bye ta\' ','2019-07-19 05:54:59',2),(6,'fborg001',NULL,'caw ta','2019-07-19 05:55:02',2),(8,'test',NULL,'test has joined the conversation.','2019-07-19 06:07:04',1),(9,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-19 06:08:17',1),(10,'fborg001',NULL,'hello','2019-07-19 06:08:49',2),(11,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-19 06:09:30',1),(12,'test',NULL,'test has joined the conversation.','2019-07-19 06:11:30',1),(13,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-19 06:22:45',1),(14,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-19 06:31:56',1),(15,'test',NULL,'test has joined the conversation.','2019-07-19 06:32:03',1),(16,'test',NULL,'hello','2019-07-19 06:32:10',2),(17,'test',NULL,'aw','2019-07-19 06:32:14',2),(18,'fborg001',NULL,'asdfasdfasd','2019-07-19 06:32:17',2),(19,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-19 07:32:57',1),(20,'test',NULL,'test has joined the conversation.','2019-07-22 06:20:56',1),(21,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-22 06:21:20',1),(22,'fborg001',NULL,'hi','2019-07-22 06:21:32',2),(23,'fborg001',NULL,'hello','2019-07-22 06:21:35',2),(24,'fborg001',NULL,'bye','2019-07-22 06:21:40',2),(25,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-22 06:21:42',3),(26,'test',NULL,'test has joined the conversation.','2019-07-22 07:46:25',1),(27,'test',NULL,'test has joined the conversation.','2019-07-22 07:47:24',1),(28,'test',NULL,'test has joined the conversation.','2019-07-22 07:48:05',1),(29,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-22 07:48:24',1),(30,'test',NULL,'test has joined the conversation.','2019-07-22 07:49:25',1),(31,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-22 07:49:36',1),(32,'test',NULL,'test has joined the conversation.','2019-07-22 08:18:24',1),(33,'test',NULL,'test has joined the conversation.','2019-07-22 08:45:24',1),(34,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-22 08:46:15',3),(35,'test',NULL,'test has joined the conversation.','2019-07-22 08:49:29',1),(36,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-22 08:49:31',1),(37,'fborg001',NULL,'fasdfasdf','2019-07-22 08:49:34',2),(38,'test',NULL,'asdfasdfasdf','2019-07-22 08:49:37',2),(39,'fborg001',NULL,'asdfadsfasdf','2019-07-22 08:49:40',2),(40,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-22 08:49:43',3),(41,'test',NULL,'test has joined the conversation.','2019-07-22 08:54:46',1),(42,'test',NULL,'test has left the conversation.','2019-07-22 08:54:47',3),(43,'test',NULL,'test has joined the conversation.','2019-07-22 08:54:48',1),(44,'test',NULL,'test has left the conversation.','2019-07-22 08:54:48',3),(45,'test',NULL,'test has joined the conversation.','2019-07-22 08:54:49',1),(46,'test',NULL,'test has left the conversation.','2019-07-22 08:54:50',3),(47,'test',NULL,'test has joined the conversation.','2019-07-23 05:40:05',1),(48,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:41:04',1),(49,'test',NULL,'test has joined the conversation.','2019-07-23 05:41:28',1),(50,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 05:41:33',3),(51,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:41:35',1),(52,'test',NULL,'test has joined the conversation.','2019-07-23 05:41:44',1),(53,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:41:46',1),(54,'test',NULL,'test has left the conversation.','2019-07-23 05:43:29',3),(55,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 05:43:45',3),(56,'test',NULL,'test has joined the conversation.','2019-07-23 05:43:58',1),(57,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:44:01',1),(58,'test',NULL,'test has left the conversation.','2019-07-23 05:44:04',3),(59,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:44:43',1),(60,'test',NULL,'test has joined the conversation.','2019-07-23 05:44:45',1),(61,'test',NULL,'test has left the conversation.','2019-07-23 05:44:49',3),(62,'test',NULL,'test has joined the conversation.','2019-07-23 05:45:40',1),(63,'test',NULL,'test has left the conversation.','2019-07-23 05:45:42',3),(64,'test',NULL,'test has joined the conversation.','2019-07-23 05:46:04',1),(65,'test',NULL,'test has left the conversation.','2019-07-23 05:46:05',3),(66,'test',NULL,'test has joined the conversation.','2019-07-23 05:49:22',1),(67,'test',NULL,'test has left the conversation.','2019-07-23 05:49:23',3),(68,'test',NULL,'test has joined the conversation.','2019-07-23 05:50:04',1),(69,'test',NULL,'test has left the conversation.','2019-07-23 05:50:05',3),(70,'test',NULL,'test has joined the conversation.','2019-07-23 05:50:46',1),(71,'test',NULL,'test has left the conversation.','2019-07-23 05:50:47',3),(72,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:51:02',1),(73,'test',NULL,'test has joined the conversation.','2019-07-23 05:51:04',1),(74,'test',NULL,'test has left the conversation.','2019-07-23 05:51:08',3),(75,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:52:04',1),(76,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 05:52:05',3),(77,'test',NULL,'test has joined the conversation.','2019-07-23 05:52:17',1),(78,'test',NULL,'test has left the conversation.','2019-07-23 05:52:18',3),(79,'test',NULL,'test has joined the conversation.','2019-07-23 05:53:25',1),(80,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:53:27',1),(81,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 05:53:34',3),(82,'test',NULL,'test has joined the conversation.','2019-07-23 05:57:00',1),(83,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:57:02',1),(84,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 05:57:05',3),(85,'test',NULL,'test has joined the conversation.','2019-07-23 05:57:14',1),(86,'test',NULL,'test has left the conversation.','2019-07-23 05:57:15',3),(87,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:57:18',1),(88,'test',NULL,'test has joined the conversation.','2019-07-23 05:57:21',1),(89,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 05:57:28',3),(90,'test',NULL,'test has left the conversation.','2019-07-23 05:57:35',3),(91,'test',NULL,'test has joined the conversation.','2019-07-23 05:57:39',1),(92,'test',NULL,'test has left the conversation.','2019-07-23 05:57:40',3),(93,'test',NULL,'test has joined the conversation.','2019-07-23 05:59:17',1),(94,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 05:59:19',1),(95,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 05:59:21',3),(96,'test',NULL,'test has joined the conversation.','2019-07-23 06:00:01',1),(97,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 06:00:03',1),(98,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 06:00:06',3),(99,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:08',1),(100,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 06:03:10',1),(101,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:22',1),(102,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:24',1),(103,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:24',1),(104,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:25',1),(105,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:26',1),(106,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:26',1),(107,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:26',1),(108,'test',NULL,'test has joined the conversation.','2019-07-23 06:03:26',1),(109,'test',NULL,'test has joined the conversation.','2019-07-23 06:06:24',1),(110,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 06:06:27',1),(111,'test',NULL,'test has left the conversation.','2019-07-23 06:06:55',3),(112,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 06:07:08',1),(113,'test',NULL,'test has joined the conversation.','2019-07-23 06:07:10',1),(114,'test',NULL,'test has left the conversation.','2019-07-23 06:07:29',3),(115,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 06:10:20',1),(116,'fborg001',NULL,'kkjjjj','2019-07-23 06:15:57',2),(117,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 06:50:25',1),(118,'test',NULL,'test has joined the conversation.','2019-07-23 06:50:28',1),(119,'test',NULL,'sdfasdfasdf','2019-07-23 06:50:31',2),(120,'fborg001',NULL,'asdfasdfasdf','2019-07-23 06:50:34',2),(121,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-23 06:50:36',3),(122,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-23 07:46:02',1),(123,'test',NULL,'test has joined the conversation.','2019-07-23 07:46:29',1),(124,'test',NULL,'kljnjkjkjk','2019-07-23 07:46:34',2),(125,'fborg001',NULL,'gvgvgvgvgv','2019-07-23 07:46:39',2),(126,'test',NULL,'test has joined the conversation.','2019-07-24 05:53:40',1),(127,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-24 05:53:59',1),(128,'fborg001',NULL,'hello','2019-07-24 05:54:03',2),(129,'test',NULL,'adfasdfasdf','2019-07-24 05:54:06',2),(130,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-24 05:54:08',3),(131,'test',NULL,'test has joined the conversation.','2019-07-24 06:00:02',1),(132,'test',NULL,'test has joined the conversation.','2019-07-24 06:01:31',1),(133,'test',NULL,'test has joined the conversation.','2019-07-24 06:05:32',1),(134,'test',NULL,'test has joined the conversation.','2019-07-24 07:36:29',1),(135,'test',NULL,'test has joined the conversation.','2019-07-24 07:37:54',1),(136,'test',NULL,'hellow ','2019-07-24 07:38:03',2),(137,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-24 07:38:23',1),(138,'fborg001',NULL,'hello','2019-07-24 07:38:29',2),(139,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-24 07:38:31',3),(140,'test',NULL,'test has joined the conversation.','2019-07-24 08:27:08',1),(141,'test',NULL,'dsfasdfadsf','2019-07-24 08:27:14',2),(142,'test',NULL,'test has left the conversation.','2019-07-24 08:30:45',3),(143,'test',NULL,'test has joined the conversation.','2019-07-24 08:52:08',1),(144,'test',NULL,'test has joined the conversation.','2019-07-24 08:52:27',1),(145,'test',NULL,'test has joined the conversation.','2019-07-24 09:00:40',1),(146,'test',NULL,'test has joined the conversation.','2019-07-24 09:06:00',1),(147,'test',NULL,'test has joined the conversation.','2019-07-24 09:06:27',1),(148,'test',NULL,'test has joined the conversation.','2019-07-24 09:06:43',1),(149,'test',NULL,'test has joined the conversation.','2019-07-24 09:12:18',1),(150,'test',NULL,'hello','2019-07-24 09:12:27',2),(151,'test',NULL,'helloooooooo','2019-07-24 09:13:53',2),(152,'test',NULL,'test has joined the conversation.','2019-07-25 05:46:25',1),(153,'test',NULL,'test has joined the conversation.','2019-07-25 05:57:47',1),(154,'test',NULL,'dfasdf','2019-07-25 05:57:52',2),(155,'joey002',NULL,'joey002 has joined the conversation.','2019-07-25 05:58:32',1),(156,'joey002',NULL,'afsdasdfasd','2019-07-25 05:58:36',2),(157,'test',NULL,'test has joined the conversation.','2019-07-25 05:58:44',1),(158,'test',NULL,'test has joined the conversation.','2019-07-25 08:28:51',1),(159,'test',NULL,'ool','2019-07-25 08:28:57',2),(160,'test',NULL,'test has joined the conversation.','2019-07-25 08:41:28',1),(161,'test',NULL,'test has left the conversation.','2019-07-25 08:41:31',3),(162,'test',NULL,'test has joined the conversation.','2019-07-25 08:41:49',1),(163,'test',NULL,'test has joined the private conversation.','2019-07-25 08:52:17',2),(164,'test',NULL,'test has joined the conversation.','2019-07-25 08:56:24',1),(165,'test',NULL,'test has joined the private conversation.','2019-07-25 08:58:40',2),(166,'test',NULL,'test has joined the private conversation.','2019-07-25 09:00:23',2),(167,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-26 05:34:45',1),(168,'test',NULL,'test has joined the conversation.','2019-07-26 05:34:56',1),(169,'fborg001',NULL,'gadfsasdf','2019-07-26 05:35:22',2),(170,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-26 05:39:56',1),(171,'test',NULL,'test has joined the conversation.','2019-07-26 05:39:59',1),(172,'test',NULL,'test has left the conversation.','2019-07-26 05:40:00',3),(173,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-26 05:40:01',3),(174,'fborg001',NULL,'fborg001 has joined the private conversation.','2019-07-26 05:40:04',2),(175,'test',NULL,'test has joined the private conversation.','2019-07-26 05:40:06',2),(176,'fborg001',NULL,'fborg001 has joined the private conversation.','2019-07-26 05:40:08',2),(177,'test',NULL,'test has joined the conversation.','2019-07-26 05:57:44',1),(178,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-26 05:58:41',1),(179,'test',NULL,'test has joined the conversation.','2019-07-26 05:59:05',1),(180,'test',NULL,'test has left the conversation.','2019-07-26 05:59:10',3),(181,'test',NULL,'test has joined the conversation.','2019-07-26 05:59:11',1),(182,'test',NULL,'test has left the conversation.','2019-07-26 05:59:12',3),(183,'test',NULL,'test has joined the conversation.','2019-07-26 05:59:12',1),(184,'test',NULL,'test has left the conversation.','2019-07-26 05:59:12',3),(185,'test',NULL,'test has joined the conversation.','2019-07-26 05:59:13',1),(186,'test',NULL,'test has left the conversation.','2019-07-26 05:59:13',3),(187,'test',NULL,'test has joined the conversation.','2019-07-26 05:59:42',1),(188,'test',NULL,'test has joined the conversation.','2019-07-26 06:00:54',1),(189,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-26 06:01:09',1),(190,'fborg001',NULL,'asdfasdfasdf','2019-07-26 06:01:12',2),(191,'test',NULL,'asdfasdfs','2019-07-26 06:01:18',2),(192,'fborg001',NULL,'fborg001 has left the conversation.','2019-07-26 06:01:22',3),(193,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-26 06:01:22',1),(194,'test',NULL,'test has left the conversation.','2019-07-26 06:02:43',3),(195,'test',NULL,'test has joined the conversation.','2019-07-26 06:02:53',1),(196,'test',NULL,'test has left the conversation.','2019-07-26 06:02:53',3),(197,'fborg001',NULL,'fborg001 has joined the conversation.','2019-07-26 06:03:10',1),(198,'test',NULL,'test has joined the conversation.','2019-07-26 06:03:12',1),(199,'test',NULL,'hello','2019-07-26 06:03:18',2),(200,'fborg001',NULL,'adsfasdfasdf','2019-07-26 06:03:21',2),(201,'test',NULL,'asdfasdfasdfsdf','2019-07-26 06:03:24',2);
/*!40000 ALTER TABLE `tbl_chat_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_chat_type`
--

DROP TABLE IF EXISTS `tbl_chat_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbl_chat_type` (
  `chat_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `chat_type` varchar(45) NOT NULL,
  PRIMARY KEY (`chat_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_chat_type`
--

LOCK TABLES `tbl_chat_type` WRITE;
/*!40000 ALTER TABLE `tbl_chat_type` DISABLE KEYS */;
INSERT INTO `tbl_chat_type` VALUES (1,'JOIN'),(2,'ONLINE'),(3,'OFFLINE');
/*!40000 ALTER TABLE `tbl_chat_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_roles`
--

DROP TABLE IF EXISTS `tbl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbl_roles` (
  `roleID` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(55) NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_roles`
--

LOCK TABLES `tbl_roles` WRITE;
/*!40000 ALTER TABLE `tbl_roles` DISABLE KEYS */;
INSERT INTO `tbl_roles` VALUES (1,'admin'),(2,'normal');
/*!40000 ALTER TABLE `tbl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tbl_users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `country` varchar(60) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `roleID` int(11) NOT NULL,
  PRIMARY KEY (`userID`),
  KEY `roleIDFK_idx` (`roleID`),
  CONSTRAINT `roleIdFK` FOREIGN KEY (`roleID`) REFERENCES `tbl_roles` (`roleID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'Joey l- administratorrr','test','','test','yBqYME7F9UpUuxGz+/OU5w==',1),(25,'Francesco ','Borg','','fborg001','MWyT/mD1+S88sqvpZL/HOw==',2),(28,'joey','jj','','joey002','ZAjNViw3mfxFUymF55pOeg==',2);
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-26  8:40:14
