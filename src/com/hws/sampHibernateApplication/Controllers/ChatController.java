 package com.hws.sampHibernateApplication.Controllers;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hws.sampHibernateApplication.Models.ChatMessage;
import com.hws.sampHibernateApplication.Models.User;
import com.hws.sampHibernateApplication.Services.ChatService;
import com.hws.sampHibernateApplication.Services.UserService;

@Controller
@ServerEndpoint(value="/chat")
public class ChatController {

	@Autowired
	private ChatService chatService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("chat")
	public ModelAndView returnChatPage(Model model,HttpSession session) {
		ModelAndView usernamesandview = new ModelAndView("chat_page");
		if(session.getAttribute("sessionId") == null) {
			model.addAttribute("warningMessageForFrontEnd","You have no right to access this");
		}
		List<User> usernames = userService.getUsernames();
		usernamesandview.addObject("usernames", usernames);
		return usernamesandview;
	}
	
	
	//MessageMapping registers the method as a message listener
	@MessageMapping("/chatRegister")
	@SendTo("/chat_topic/public")
	public ChatMessage register(@Payload ChatMessage message) {
		message.setDateTime(getTimeDate());
		chatService.insertMessage(message);
		return message;
	}
	
	@MessageMapping("/chatSend")
	@SendTo("/chat_topic/public")
	public ChatMessage sendMessage(@Payload ChatMessage message) {
		message.setDateTime(getTimeDate());
		chatService.insertMessage(message);
		return message;
	}
	
	private Date getTimeDate() {
		Date currentDateTime = new Date();
		return currentDateTime;
	}
}
