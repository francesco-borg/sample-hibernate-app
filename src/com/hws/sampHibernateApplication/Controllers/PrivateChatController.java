package com.hws.sampHibernateApplication.Controllers;

import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hws.sampHibernateApplication.Models.ChatMessage;
import com.hws.sampHibernateApplication.Services.ChatService;

@Controller
@ServerEndpoint(value="/private_chat")
public class PrivateChatController {
	@Autowired
	private ChatService chatService;
	
	@RequestMapping("private_chat")
	public ModelAndView returnChatPage(Model model,HttpSession session) {
		ModelAndView chatwindow = new ModelAndView("private_chat_window");
		if(session.getAttribute("sessionId") == null) {
			model.addAttribute("warningMessageForFrontEnd","You have no right to access this");
		}
		return chatwindow; 
	}
	
	
	
	private Date getTimeDate() {
		Date currentDateTime = new Date();
		return currentDateTime;
	}
}