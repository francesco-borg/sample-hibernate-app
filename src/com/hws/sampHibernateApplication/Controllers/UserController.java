package com.hws.sampHibernateApplication.Controllers;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hws.sampHibernateApplication.Models.Role;
import com.hws.sampHibernateApplication.Models.User;
import com.hws.sampHibernateApplication.Services.UserService;
import com.hws.sampHibernateApplication.Utils.LogMessages;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	static LogRecord logRecord;
	LogMessages logMessages = new LogMessages();
	private static final Logger logger = Logger.getLogger(UserController.class.getName()); 
	
	@RequestMapping("/")
	public String Home() {
		return "home";
	}
	
	@RequestMapping("/getUsers")
	public ModelAndView getUsers(Model model, HttpSession session) {
		ModelAndView usersAndView = new ModelAndView("userlist");
		if(session.getAttribute("sessionId") == null)
			model.addAttribute("warningMessageForFrontEnd","You have no right to access this");
		else
		{
			List<User> users = userService.getUsers();
			if(!users.isEmpty())
				usersAndView.addObject("users", users);
			else
				model.addAttribute("warningMessageForFrontEnd", "No registered users are found in the database.");
		}
		//returning the model and the view
		return usersAndView;
	}
	
	@RequestMapping("/viewUser/{userID}")
	public ModelAndView viewUser(Model modelContainer, @PathVariable("userID") int userID, HttpSession session) {
		User userToBeUpdated = userService.getUserById(userID);
		
		if(session.getAttribute("loggedInUser").equals(userToBeUpdated.getUsername()) 
				|| session.getAttribute("userRole").equals("admin") &&
				session.getAttribute("sessionId").equals("AUTHORIZED")) 
		{
			modelContainer.addAttribute("userToBeUpdated", userToBeUpdated);
			return new ModelAndView("updateUser");
		}
		modelContainer.addAttribute("errorMessageForFrontEnd","You have no permission to access this");
		return new ModelAndView("error");
	}
	
	@PostMapping("viewUser/updateUser/{userID}")
	public ModelAndView updateUser(ModelMap updatedUser,Model modelContainer, 
			@ModelAttribute("updateUserForm") User user, @PathVariable("userID") int userID,
			HttpSession session) {
		List<User> updatedUserList = userService.updateUser(user);
		updatedUser.addAttribute("users", updatedUserList);
		session.setAttribute("loggedInUser", user.getUsername());
		//When the process of updating is finished, the website will redirect to the get users list
		//with the updated changes.
		return new ModelAndView("forward:/getUsers");
	}
	
	@RequestMapping("/deleteUser/{userID}")
	public ModelAndView deleteUser(Model modelContainer, HttpSession session, @PathVariable("userID") int userID) {
		//this will return the updated user's list without the deleted user
		ModelAndView updatedUsersAndView = new ModelAndView("userlist");
		
		List<Integer> userIdList = 
				userService.getUserIdByUserName(session.getAttribute("loggedInUser").toString());
		int userToBeDeletedId = userIdList.get(0);
		
		/*if the user is admin or a normal user then continue */
		if(session.getAttribute("userRole").equals("admin") || userID == userToBeDeletedId) {
			List<User> users = userService.deleteUser(userID);
			updatedUsersAndView.addObject("users", users);
			if(!session.getAttribute("userRole").equals("admin")) 
				return new ModelAndView("forward:/logout");
			return updatedUsersAndView;
		}
		else 
			modelContainer.addAttribute("errorMessageForFrontEnd","You have no permission to access this");
		return new ModelAndView("error");
	}

	@RequestMapping("/register")
	public String register() {
		return "register";
	}
	
	//Here I am getting the data from the registration form
	@RequestMapping(value = "/register", method=RequestMethod.POST)
	public  String registerUser(@ModelAttribute("registerUserForm") User user, Model model) {
		try {
			if("".equals(user.getPassword()) || "".equals(user.getUsername()) || 
					"".equals(user.getFirstName()) || "".equals(user.getLastName()) || 
					!userService.insertUser(user)) 
						model.addAttribute("userRegistered", false);
				 else 
					model.addAttribute("userRegistered", true);	
			} catch (Exception e) {
				logMessages.logError(logger, e.getMessage(), Level.SEVERE);
				return "error";
			}
			return "register";
		}
		
	@RequestMapping("/login")
	public String login() {
		return "login-user";
	}
	
	//getting the data from the login form
	@PostMapping("loginUser")
	public String loginUser(@ModelAttribute("loginUserForm") User user, Model model, HttpSession session) {
		/*If the login cred.. were entered incorrectly this if condition will result to true and 
		 * the error page is returned. */		
		try {
			if(userService.getUser(user).isEmpty()) {	
				logMessages.logError(logger, "The account does not exist!", Level.INFO);
				model.addAttribute("userLoggedIn", false);
				return "login-user";
			} else {
				List<Role> loggedInUserRole = userService.GetLoggedInUserRole(user.getUsername());
				session.setAttribute("sessionId", "AUTHORIZED");
				session.setAttribute("loggedInUser", user.getUsername());
				session.setAttribute("userRole",loggedInUserRole.get(0).getRole());
			}
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
			return "error";
		}		
		return "home";
	}
	
	@RequestMapping("logout")
	public String logoutUser(HttpSession session) {
		/*When the user logs out, I'm removing access from the get users list*/
		session.removeAttribute("sessionId");
		session.removeAttribute("loggedInUser");
		return "home";
	}
}
