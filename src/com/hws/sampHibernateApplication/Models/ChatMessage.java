package com.hws.sampHibernateApplication.Models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_chat_log", schema="sampleapp")
public class ChatMessage {

	private int chat_id;
	private String message;
	private ChatType chat_type;
	private String to_username;
	private String from_username;
	private Date dateTime;

	
	public ChatMessage() {
		
	}
	
	public ChatMessage(String to_username,String from_username, String message, ChatType msgProtocol) {
		super();
		this.to_username = to_username;
		this.from_username = from_username;
		this.message = message;
		this.chat_type = msgProtocol;
	}

	@Id // Since in the db it's primary key
	@GeneratedValue(strategy=GenerationType.IDENTITY) // Since in the db it's auto inc.. 
	@Column(name="chat_id")
	public int getChat_id() {
		return chat_id;
	}
	
	@Column(name="to_username")
	public String getTo_username() {
		return to_username;
	}
	
	@Column(name="from_username")
	public String getFrom_username() {
		return from_username;
	}
	
	@Column(name="chat_message")
	public String getMessage() {
		return message;
	}
	
	
	
	@Column(name="date_time")
	public Date getDateTime() {
		return dateTime;
	}

	

	@ManyToOne
	@JoinColumn(name="chat_type_id")
	public ChatType getChat_type() {
		return chat_type;
	}
	
	
	
	public void setChat_id(int chat_id) {
		this.chat_id = chat_id;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public void setChat_type(ChatType chat_type) {
		this.chat_type = chat_type;
	}

	public void setTo_username(String to_username) {
		this.to_username = to_username;
	}

	public void setFrom_username(String from_username) {
		this.from_username = from_username;
	}
	
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
}
