package com.hws.sampHibernateApplication.Models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="tbl_chat_type", schema="sampleapp")
public class ChatType {
	
	public ChatType() {
		
	}
	
	public ChatType(int chatTypeId, String chatType) {
		this.chatTypeId = chatTypeId;
		this.chat_type = chatType;
	}

	private int chatTypeId;
	private String chat_type;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="chat_type_id")
	public int getChatTypeId() {
		return chatTypeId;
	}

	public void setChatTypeId(int chatTypeId) {
		this.chatTypeId = chatTypeId;
	}
	
	@Column(name="chat_type")
	public String getChat_type() {
		return chat_type;
	}

	public void setChat_type(String chat_type) {
		this.chat_type = chat_type;
	}
}
