package com.hws.sampHibernateApplication.Models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tbl_roles", schema="sampleapp")
public class Role {
	
	
	private int roleID;
	private String role;

	
	private Set<User> userRoles;
	
	public Role() {
		
	}
	
	public Role(int roleId, String role, Set<User> userRoles) {
		this.roleID = roleId;
		this.role = role;
		this.userRoles = userRoles;
	}
	
	@Override
	public String toString() {
		return "role=" + role;
	}

	@OneToMany(mappedBy="role")
	public Set<User> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<User> userRoles) {
		this.userRoles = userRoles;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="roleID")
	public int getRoleID() {
		return roleID;
	}

	public void setRoleID(int roleId) {
		this.roleID = roleId;
	}

	
	@Column(name="role")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}


}
