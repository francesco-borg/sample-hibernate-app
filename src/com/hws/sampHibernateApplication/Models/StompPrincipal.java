package com.hws.sampHibernateApplication.Models;

import java.security.Principal;

public class StompPrincipal implements Principal {
	String username;
	
	
	public StompPrincipal(String username) {
		super();
		this.username = username;
	}


	@Override
	public String getName() {
		return username;
	}

}
