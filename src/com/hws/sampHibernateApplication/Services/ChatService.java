package com.hws.sampHibernateApplication.Services;

import java.util.List;

import com.hws.sampHibernateApplication.Models.ChatMessage;

public interface ChatService {
	public boolean insertMessage(ChatMessage message);
	public List<ChatMessage> getChatMsgs();
}
