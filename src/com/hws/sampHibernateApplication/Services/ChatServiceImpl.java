package com.hws.sampHibernateApplication.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hws.sampHibernateApplication.Models.ChatMessage;
import com.hws.sampHibernateApplication.dao.ChatDAO;

@Service
public class ChatServiceImpl implements ChatService {

	@Autowired
	private ChatDAO chatDAO;
	
	@Override
	public boolean insertMessage(ChatMessage message) {
		return chatDAO.insertMessage(message);
	}

	@Override
	public List<ChatMessage> getChatMsgs() {
		return chatDAO.getChatMsgs();
	}

}
