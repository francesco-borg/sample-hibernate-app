package com.hws.sampHibernateApplication.Services;

import java.util.List;

import com.hws.sampHibernateApplication.Models.Role;
import com.hws.sampHibernateApplication.Models.User;

public interface UserService {
	public List<User> getUsers();
	public List<User> getUser(User user);
	public User getUserById(int userID);
	public boolean insertUser(User user);
	public List<User> deleteUser(int userID);
	public List<User> updateUser(User user);
	public List<Integer> getUserIdByUserName(String userName);
	public List<Role> GetLoggedInUserRole(String userName);
	public List<User> getUsernames();
}
