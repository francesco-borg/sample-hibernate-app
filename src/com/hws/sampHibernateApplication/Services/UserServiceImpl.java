package com.hws.sampHibernateApplication.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hws.sampHibernateApplication.Models.Role;
import com.hws.sampHibernateApplication.Models.User;
import com.hws.sampHibernateApplication.dao.UserDAO;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;
	
	@Override
	public List<User> getUsers() {
		return userDAO.getUsers();
	}
	
	@Override
	public List<User> getUser(User user) {
		return userDAO.getUser(user);
	}

	@Override
	public boolean insertUser(User user) {
		return userDAO.insertUser(user);
		
	}

	@Override
	public List<User> updateUser(User user) {
		return userDAO.updateUser(user);
	}

	@Override
	public List<User> deleteUser(int userID) {
		return userDAO.deleteUser(userID);
		
	}

	@Override
	public User getUserById(int userID) {
		return userDAO.getUserById(userID);
	}

	@Override
	public List<Integer> getUserIdByUserName(String userName) {
		return userDAO.getUserIdByUserName(userName);
	}

	@Override
	public List<Role> GetLoggedInUserRole(String username) {
		return userDAO.GetLoggedInUserRole(username);
	}
	
	@Override
	public List<User> getUsernames() {
		return userDAO.getUsernames();
	}

}
