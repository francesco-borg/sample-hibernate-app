package com.hws.sampHibernateApplication.TestingApp;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.hws.sampHibernateApplication.Models.User;
import com.hws.sampHibernateApplication.Services.UserServiceImpl;
import com.hws.sampHibernateApplication.dao.UserDAOImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTestCases {	

	User dummyUser = 
			new User("dummyUserName","dummyUserSurname","dummy","dummy",null);
	User dummyUser2 = 
			new User("dummyUserName255","dummyUserSurname5534","dummy","dummy",null);
	List<User> registeredUsers = new ArrayList<User>();
	List<User> loggedInUser = new ArrayList<User>();
	
	@InjectMocks
	private UserServiceImpl userService;
	
	@Mock
	private UserDAOImpl userDAO;
		
	@Test
	public void registerNewUser() {				
		when(userDAO.insertUser(dummyUser)).thenReturn(true);
		Boolean registrationGood = userDAO.insertUser(dummyUser);
		assertEquals(registrationGood, true);
	}
	
	/*Immagine you have this scenario;
	 * user A registers with the username test123, then later
	 * user B registers with the same username as user A
	 */
	@Test
	public void WhenANewUserRegistersWithSameUsernameTest() {
		when(userDAO.insertUser(dummyUser)).thenReturn(false);
		Boolean testOutcome = userDAO.insertUser(dummyUser);
		assertEquals(false, testOutcome);
	}
	
	@Test
	public void loginUserTest() {
		loggedInUser.add(dummyUser);
		when(userDAO.getUser(dummyUser)).thenReturn(loggedInUser);
		List<User> loggedInUserDb = userService.getUser(dummyUser);
		assertThat(loggedInUserDb, is(sameInstance(loggedInUser)));
	}

	/*Getting all users from database*/
	@Test
	public void gettingUsersTest() {
		registeredUsers.add(dummyUser);
		registeredUsers.add(dummyUser2);
		when(userDAO.getUsers()).thenReturn(registeredUsers);
		List<User> registeredUsersDb = userService.getUsers();
		assertThat(registeredUsersDb, is(sameInstance(registeredUsers)));
	}
	
		
}
