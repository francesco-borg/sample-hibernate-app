package com.hws.sampHibernateApplication.TestingApp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.hws.sampHibernateApplication.Models.User;
import com.hws.sampHibernateApplication.Services.UserService;
import com.hws.sampHibernateApplication.dao.UserDAO;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTestCasesMockito {
	User dummyUser = new User();	
	
	@Mock
	private UserService userService;
	
	@InjectMocks
	private UserDAO userDAO; 

	/*Imagine you have this scenario;
	 * user A registers with the username test123, then later
	 * user B registers with the same username as user A
	 */
	@Test
	public void WhenANewUserRegistersWithSameUsernameTest() {
		dummyUser.setFirstName("test55");
		dummyUser.setLastName("test8");
		dummyUser.setPassword("test55");
		dummyUser.setUsername("test55");
		Mockito.when(userDAO.insertUser(dummyUser)).thenReturn(false);
		Assert.assertEquals(false, userService.insertUser(dummyUser));
	}
	
}
