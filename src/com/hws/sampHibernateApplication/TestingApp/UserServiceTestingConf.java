package com.hws.sampHibernateApplication.TestingApp;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import com.hws.sampHibernateApplication.Services.UserService;


@Profile("test")
@Configuration
public class UserServiceTestingConf {
	@Bean
	@Primary
	public UserService userServiceTest() {
		return Mockito.mock(UserService.class);
	}
}
