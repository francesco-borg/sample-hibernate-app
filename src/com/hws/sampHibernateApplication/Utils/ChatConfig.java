package com.hws.sampHibernateApplication.Utils;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

@Configuration
@EnableWebSocketMessageBroker
public class ChatConfig implements WebSocketMessageBrokerConfigurer { 

	@Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/chat_topic", "/queue/chatuser");
        config.setApplicationDestinationPrefixes("/sampHibernateApplication");
    } 
	
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        //registering the channel
    	registry.addEndpoint("/chat").setAllowedOrigins("*").withSockJS();
    	registry.addEndpoint("/private_chat")
    	.setAllowedOrigins("*")
    	.setHandshakeHandler(new DefaultHandshakeHandler() {
    		public boolean beforeHandshake(
    				ServerHttpRequest req,
    				ServerHttpResponse resp,
    				WebSocketHandler wsHandler,
    				Map attr) {
    			
    			if(req instanceof ServletServerHttpRequest ) {
    				ServletServerHttpRequest servletRequest
                    = (ServletServerHttpRequest) req;
                   HttpSession session = servletRequest
                     .getServletRequest().getSession();
                   attr.put("sessionId", session.getId());
    			}
    			return true;
    			
    		}
    		
    	})
    	.withSockJS();
    }
}
