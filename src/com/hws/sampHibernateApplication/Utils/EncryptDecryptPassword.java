package com.hws.sampHibernateApplication.Utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class EncryptDecryptPassword {

	static XmlWriter xmlWriter;
	static LogRecord logRecord;
	private static SecretKeySpec secretKey;
	private static byte[] key;
	
	static LogMessages logMessages = new LogMessages();
	
	private static final Logger logger = Logger.getLogger(EncryptDecryptPassword.class.getName());

	public EncryptDecryptPassword(String keyString) {
		setKey(keyString);
	}
	
	private static void setKey(String myKey) {
		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			//hash the key using sha 1 algorithm
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key,"AES");
		} catch (NoSuchAlgorithmException e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		
		} catch(UnsupportedEncodingException e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
	}
	
	//Encryption method using AES
	public String encrypt(String password) {
		try {
			//get an instance of aes cipher
			Cipher cipher = Cipher.getInstance("AES");
			//setting the cipher to encrypt mode, since I am encrypting the password
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			//Encryption
			return Base64.getEncoder().encodeToString(cipher.doFinal(password.getBytes("UTF-8")));
		} catch (Exception e) {
			
			//in case of an error log it in the xml file
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		
		return "error";
	}
	
	//same story as above but instead of encryption, i am decrypting the encrypted password from the database
	public String decrypt(String password) {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return new String(cipher.doFinal(Base64.getDecoder().decode(password)));
		} catch (Exception e) {
			logMessages.logError(logger,e.getMessage(), Level.SEVERE);
		}
		return "error";
	}
	
}
