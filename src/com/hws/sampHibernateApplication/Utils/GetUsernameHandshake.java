package com.hws.sampHibernateApplication.Utils;

import java.security.Principal;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import com.hws.sampHibernateApplication.Models.StompPrincipal;

public class GetUsernameHandshake extends DefaultHandshakeHandler {
	
	protected Principal getUsername(ServerHttpRequest req, WebSocketHandler handler, 
			Map<String,Object> attr) {
				return new StompPrincipal(UUID.randomUUID().toString());
		
	}
}
