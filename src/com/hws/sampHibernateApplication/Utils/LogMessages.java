package com.hws.sampHibernateApplication.Utils;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class LogMessages {
	
	static LogRecord logRecord;
	public void logError(Logger logger, String errorMess, Level level) {
		FileHandler handler;
		try {
			//i'm creating a filehandler so that I would be able
			//to write to an xml file
			handler = new FileHandler();
			handler.setFormatter(new XmlWriter());
			//create a new instance of the formatter XmlWriter
			//and store it in a var called formatter
			Formatter formatter = handler.getFormatter();
			logRecord = new LogRecord(level, errorMess);
			//logging the error to the console
			logger.log(logRecord);
			//write the error mess with the timestamp in an xml file
			// C:\eclipse\errors.xml
			formatter.format(logRecord);
		} catch (SecurityException e) {
			//Let's pray to God that the program don't enter the catch 
			//clause; 
			
			//if the program enters the catch clause; then something 
			//wrong happened while creating/appending/reading the file etc.
			//and must be handled somehow
			logger.log(Level.SEVERE, e.getMessage());
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
	}
}
