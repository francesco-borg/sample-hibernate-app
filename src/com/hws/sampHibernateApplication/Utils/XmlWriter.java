package com.hws.sampHibernateApplication.Utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlWriter extends Formatter {
	
	private static final Logger logger = Logger.getLogger(XmlWriter.class.getName());
	
	
	
	@Override
	public String format(LogRecord err) {
		
		//saving the file in the eclipse installation directory -- in the future this would be changed
		//to the project directory..
		final String xmlFilePath = new File("").getAbsolutePath() + "\\errors.xml";
		File errorFile = new File(xmlFilePath);
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        try {
        	DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            //if the errorfile xml file does not exist, then a new xml file would be created
        	if(errorFile.exists() == false) {
            	Document doc = documentBuilder.newDocument();
            	writeToXml(doc,err,xmlFilePath);
            	
            }
            else {
            	//if the errorfile xml file exists the directory, then that file
            	//will be used, and the new xml would be appended
            	Document doc = documentBuilder.parse(xmlFilePath);
            	AppendToXml(doc,err,xmlFilePath);
            }
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
        return "";
	}
	
	
	/*The difference between writeToXml and appendToXml is in writeToXml I'm creating
	 * the errors root which is the beginning of an xml file
	 * and in the second method appendToXml I'm just getting the root of the xml document by
	 * calling doc.getDocumentElement()*/
	public void writeToXml(Document doc, LogRecord err, String xmlFilePath) {
		try {
			
			Element errorsRoot = doc.createElement("errors");
			doc.appendChild(errorsRoot);
			
			Element errorElm = doc.createElement("error");
			errorsRoot.appendChild(errorElm);
			
			Attr errorTypeAttr = doc.createAttribute("type");
			errorTypeAttr.setValue(err.getLevel().toString());
			errorElm.setAttributeNode(errorTypeAttr);
			
			Attr errorMessAttr = doc.createAttribute("message");
			errorMessAttr.setValue(err.getMessage().toString());
			errorElm.setAttributeNode(errorMessAttr);
			
			Attr timestampAttr = doc.createAttribute("timestamp");
			timestampAttr.setValue(calcDate(err.getMillis()));
			errorElm.setAttributeNode(timestampAttr);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT,"yes");
            DOMSource domSource = new DOMSource(doc);
            
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));
            //StreamResult consoleResult = new StreamResult(System.out);
            //transformer.transform(domSource, consoleResult);
            transformer.transform(domSource, streamResult);
      	
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
	}
	
	
	
	public void AppendToXml(Document doc, LogRecord err, String xmlFilePath) {
		try {
			Element xmlRoot = doc.getDocumentElement();
			
			Element errorElm = doc.createElement("error");
			
			Attr errorTypeAttr = doc.createAttribute("type");
			errorTypeAttr.setValue(err.getLevel().toString());
			errorElm.setAttributeNode(errorTypeAttr);
			
			Attr errorMessAttr = doc.createAttribute("message");
			errorMessAttr.setValue(err.getMessage().toString());
			errorElm.setAttributeNode(errorMessAttr);
			
			Attr timestampAttr = doc.createAttribute("timestamp");
			timestampAttr.setValue(calcDate(err.getMillis()));
			errorElm.setAttributeNode(timestampAttr);

			xmlRoot.appendChild(errorElm);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT,"yes");
            DOMSource domSource = new DOMSource(doc);
            
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));
            //StreamResult consoleResult = new StreamResult(System.out);
            //transformer.transform(domSource, consoleResult);
            transformer.transform(domSource, streamResult);
      	
		} catch (Exception e) {
			logger.log(Level.SEVERE, e.getMessage());
		}
	}
	
	
	private String calcDate(long milliSecs) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("d MM yyy, hh:mm:ss");
		Date resDate = new Date(milliSecs);
		return dateFormat.format(resDate);
	}
}
