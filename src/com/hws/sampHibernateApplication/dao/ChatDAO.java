package com.hws.sampHibernateApplication.dao;

import java.util.List;

import com.hws.sampHibernateApplication.Models.ChatMessage;

public interface ChatDAO {
	public boolean insertMessage(ChatMessage message);
	public List<ChatMessage> getChatMsgs();
}
