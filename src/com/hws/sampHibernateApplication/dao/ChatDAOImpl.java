package com.hws.sampHibernateApplication.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import com.hws.sampHibernateApplication.Models.ChatMessage;
import com.hws.sampHibernateApplication.Models.ChatType;
import com.hws.sampHibernateApplication.Utils.LogMessages;

@Repository
public class ChatDAOImpl implements ChatDAO {

	private SessionFactory sessionFactory = new Configuration()
			.configure()
			.addAnnotatedClass(ChatDAO.class)
			.buildSessionFactory();
	
	Session chatSession = sessionFactory.getCurrentSession();
	
	static LogRecord logRecord;
	LogMessages logMessages = new LogMessages();
	private static final Logger logger = Logger.getLogger(ChatDAOImpl.class.getName());
	
	private void SessionIsItOpen() {
		try {
			if(chatSession.isOpen() != true) {
				chatSession = sessionFactory.openSession();
			}
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(),Level.SEVERE);
		}
	}
	
	@SuppressWarnings("unchecked")
	private List<ChatType> getChatType() {
		SessionIsItOpen();
		List<ChatType> chatTypes = new ArrayList<ChatType>();
		try {
			chatSession.beginTransaction();
			chatTypes = chatSession.createQuery("from ChatType").getResultList();
			chatSession.getTransaction().commit();
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		
		return chatTypes;
	}
	
	@Override
	public boolean insertMessage(ChatMessage message) {
		try {
			List<ChatType> chatTypes = getChatType();
			SessionIsItOpen();
			if(message.getMessage().contains("has joined the conversation")) {
				message.setChat_type(chatTypes.get(0));
			}
			else if(message.getMessage().contains("has left the conversation")) {
				message.setChat_type(chatTypes.get(2));
			} else {
				message.setChat_type(chatTypes.get(1));
			}
			chatSession.beginTransaction();
			chatSession.persist(message);
			chatSession.getTransaction().commit();
			return true;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		chatSession.getTransaction().commit();
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ChatMessage> getChatMsgs() {
		List<ChatMessage> chatMessages = new ArrayList<ChatMessage>();
		SessionIsItOpen();
		try {
			chatSession.beginTransaction();
			chatMessages = chatSession.createQuery("select username,chat_message,"
					+ " chat_type, date_time, chat_type from ChatMessage").getResultList();
			chatSession.getTransaction().commit();
			
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		return chatMessages;
	}

}
