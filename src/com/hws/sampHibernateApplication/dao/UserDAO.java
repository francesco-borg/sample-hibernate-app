package com.hws.sampHibernateApplication.dao;

import java.util.List;
import com.hws.sampHibernateApplication.Models.User;
import com.hws.sampHibernateApplication.Models.Role;

public interface UserDAO {
	public List<User> getUsers();
	public List<User> getUser(User user);
	public User getUserById(int userID);
	public boolean insertUser(User user);
	public List<User> deleteUser(int userID);
	public List<User> updateUser(User user);
	public List<Integer> getUserIdByUserName(String userName);
	public List<Role> GetLoggedInUserRole(String username);
	public List<User> getUsernames();
}
