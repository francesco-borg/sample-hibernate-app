package com.hws.sampHibernateApplication.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.hws.sampHibernateApplication.Models.Role;
import com.hws.sampHibernateApplication.Models.User;
import com.hws.sampHibernateApplication.Utils.EncryptDecryptPassword;
import com.hws.sampHibernateApplication.Utils.LogMessages;

@Repository
public class UserDAOImpl implements UserDAO {

	/*The session factory is responsible for connecting to the database by pulling out
	 * the connection url, port, username and password from hibernate.cfg.xml*/
	private SessionFactory sessionFactory = new Configuration()
				.configure()
				.addAnnotatedClass(User.class)
				.buildSessionFactory();
	
	Session userSession = sessionFactory.getCurrentSession();
	EncryptDecryptPassword encryptionDecryption;
	public UserDAOImpl() {
		encryptionDecryption = new EncryptDecryptPassword("hibernate_app");
	}
	
	static LogRecord logRecord;
	LogMessages logMessages = new LogMessages();
	private static final Logger logger = Logger.getLogger(UserDAOImpl.class.getName());
	
	private String passwordEncryption(User user) {
		String encryptedInputtedPass = encryptionDecryption.encrypt(user.getPassword());
		return encryptedInputtedPass;
	}
	
	/*These methods were made to reduce the amount of line of codes in the other methods
	 * like getUsers and getUser methods -- CODE REUSABILITY AT IT'S BEST :)*/
	@SuppressWarnings("unchecked")
	/*The suppresswarning was made to ignore the the type safety warning: 
	 * the expression of type list needs unchecked conversion to conform to list<user>*/
	private List<User> getUsersQry() {
		try {
			userSession.beginTransaction();
			List<User> userList = (List<User>) userSession.createQuery("from User").getResultList();
			userSession.getTransaction().commit();
			if(userList.isEmpty()) {
				return new ArrayList<User>();
			}
			return userList;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(),Level.SEVERE);
		}
		return new ArrayList<User>();
	}
	
	@SuppressWarnings("unchecked")
	private List<Role> getRolesQry() {
		List<Role> roleList = new ArrayList<Role>();
		try {
			SessionIsItOpen();
			userSession.beginTransaction();
			roleList = (List<Role>) userSession.createQuery("from Role").getResultList();
			userSession.getTransaction().commit();
			if(roleList.isEmpty()) {
				return new ArrayList<Role>();
			}
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(),Level.SEVERE);
		}
		return roleList;
	}
	
	@SuppressWarnings("unchecked")
	private List<Role> getLoggedInUserRoleQry(String username) {
		try {
			SessionIsItOpen();
			userSession.beginTransaction();
			Query qry = userSession.createQuery("select role from User where username = :username");
			qry.setParameter("username", username);
			List<Role> loggedInUserRoleList = qry.getResultList(); 
			userSession.getTransaction().commit();
			return loggedInUserRoleList;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		return new ArrayList<Role>();
	}
	
	@SuppressWarnings("unchecked")
	private List<User> getUserQry(User user) {
		//getting the username from the model
		List<User> returnedUser = new ArrayList<User>();
		SessionIsItOpen();
		try {
			Query qry = userSession.createQuery("select userID,firstName, lastName, country, "
					+ "username,role from User where username = :username");
			qry.setParameter("username", user.getUsername());
			userSession.beginTransaction();
			returnedUser = qry.getResultList(); 
			userSession.getTransaction().commit();
		} catch(Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}		
		return returnedUser;
	}
	
	@SuppressWarnings("unchecked")
	private List<User> getUserByUsername(String username) {
		List<User> existingUser = new ArrayList<User>();
		try {
			SessionIsItOpen();
			userSession.beginTransaction();
			Query qry = userSession.createQuery("select username from User where username = :username");
			qry.setParameter("username", username);
			existingUser = qry.getResultList();
			userSession.getTransaction().commit();
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		return existingUser;
	}

	private boolean insertUserQry(User user) {
		try {
			if(encryptionDecryption.encrypt(user.getPassword()) != "error") {
				user.setPassword(encryptionDecryption.encrypt(user.getPassword()));
				SessionIsItOpen();
				userSession.beginTransaction();
				userSession.persist(user);
				userSession.getTransaction().commit();
				return true;
			} 	
		} catch (Exception e) {
			if(e.getClass().getCanonicalName() == "java.lang.sqlexception") {
				logMessages.logError(logger, e.getMessage(), Level.WARNING);
			} else {
				logMessages.logError(logger, e.getMessage(), Level.SEVERE);
			}
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private List<User> getUsernamesQry() {
		try {
			userSession.beginTransaction();
			List<User> usernames = userSession.createQuery("select username from User").getResultList();
			userSession.getTransaction().commit();
			return usernames;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		
		return new ArrayList<User>();
	}
		
	@SuppressWarnings("unchecked")
	private List<User> getUserPasswordQry(User user) {
		try {
			userSession.beginTransaction();
			Query qry = userSession.createQuery("select password from User where username = :username");
			qry.setParameter("username", user.getUsername());
			List<User> userPass = qry.getResultList();
			return userPass;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(),Level.SEVERE);
		}
		
		return new ArrayList<User>();
	}
	
	private boolean settingData(User oldUserData, User newUserData) {
		try {
			oldUserData.setFirstName(newUserData.getFirstName());
			oldUserData.setLastName(newUserData.getLastName());
			oldUserData.setCountry(newUserData.getCountry());
			oldUserData.setUsername(newUserData.getUsername());
			return true;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.WARNING);
		}
		return false;
	}
	
	private void SessionIsItOpen() {
		try {
			if(userSession.isOpen() != true) {
				userSession = sessionFactory.openSession();
			}
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(),Level.SEVERE);
		}
	}
	
	public void deleteUserFromDb(Session session, int userID) {
		/*From the view I got the id of the user to be deleted, i'm passing to this method,
		 * then by using that id  i'm getting the user object, and I passed it to the session.delete */
		try {
			User userToBeDeleted = getUserById(userID);
			session.delete(userToBeDeleted);
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(),Level.WARNING);
		}
		
	}
	
	/*Transactional annotations are used to commit when everything is successful and rolling back
	 * if anything goes wrong.*/
	@Override
	@Transactional
	public List<User> getUsers() {
		try {
			SessionIsItOpen();
			return getUsersQry();
		} catch(Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}	
		return new ArrayList<User>();
	}
	
	
	public List<User> getUsernames() {
		try {
			SessionIsItOpen();
			return getUsernamesQry();
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		
		return new ArrayList<User>();
	}
	
	@Transactional
	public String getUserPassword(User user) {
		try {
			//userSession = sessionFactory.openSession();
			/*Since I'm getting the passwordkey from the database in this format
			 * [passwordkey], i needed to replace any curly braces to spaces, and remove
			 * those spaces with the trim method. I could did that with substring
			 * but the passwordkey could be 12 chars or 20 chars so substring was useless*/
			SessionIsItOpen();
			return getUserPasswordQry(user)
					.toString().replace('[', ' ').replace(']', ' ').trim();
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		return "";
	}
	
	public List<User> getUser(User user) {
		try {
			SessionIsItOpen();
			/* get the password from the model i.e login form encrypt it 
			 * and compare the encrypted password with the one of the db
			 * and if it is not equal i'm returning an empty array list*/
			if(passwordEncryption(user).equals(getUserPassword(user)) == false) {
				userSession.getTransaction().commit();
				return new ArrayList<User>();
			}
			userSession.getTransaction().commit();
			return getUserQry(user);
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		} 
		return new ArrayList<User>();
	}
	
	public User getUserById(int userId) {
		User user = new User();
		try {
			SessionIsItOpen();
			user = userSession.get(User.class, userId);
			return user;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		return user;
	}
	public boolean insertUser(User user) {
		Role role;
		try {
			SessionIsItOpen();
			List<User> existingUser = getUserByUsername(user.getUsername());
			if(existingUser.isEmpty()) {
				role = getRolesQry().get(1);
				user.setRole(role);
				return insertUserQry(user);
			}
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		return false;
	}

	@Override
	public List<User> deleteUser(int userID) {
		try {
			SessionIsItOpen();
			userSession.beginTransaction();
			deleteUserFromDb(userSession, userID);
			userSession.getTransaction().commit();
			return getUsers();
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		return new ArrayList<User>();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getUserIdByUserName(String userName) {
		try {
			SessionIsItOpen();
			Query qry = userSession.createQuery("select userID from User where username = :username");
			qry.setParameter("username", userName);
			List<Integer> user = qry.getResultList();
			return user;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.SEVERE);
		}
		
		return new ArrayList<Integer>();
	}
	@Override
	public List<User> updateUser(User user) {
		try {
			SessionIsItOpen();
			userSession.beginTransaction();
			User oldUserData = getUserById(user.getUserID());
			settingData(oldUserData, user);
			userSession.getTransaction().commit();
			List<User> updatedUserList = getUsers();
			return updatedUserList;
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.WARNING);
		}
		return new ArrayList<User>();
	}

	@Override
	public List<Role> GetLoggedInUserRole(String username) {
		try {
			SessionIsItOpen();
			return getLoggedInUserRoleQry(username);
			
		} catch (Exception e) {
			logMessages.logError(logger, e.getMessage(), Level.WARNING);
		}
		
		return new ArrayList<Role>();
	}
}