package com.hws.sampHibernateApplication.jdbc;

import java.sql.DriverManager;
import java.io.File;
import java.sql.Connection;

public class testJdbc {

	public static void main(String[] args) {
		String jdbcUrl = "jdbc:mysql://localhost:3306/sampleApp?serverTimezone=UTC";
		String userName = "root";
		String pass = "admin";
		String filePath = new File("").getAbsolutePath();
		filePath.concat("hello.txt");
		
		try {
		System.out.println("Connecting to the db" + jdbcUrl);	
		Connection sqlConn =
				DriverManager.getConnection(jdbcUrl, userName, pass);
		
		System.out.println("Connection established!");
		sqlConn.close();
		
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
